<?php
// Create table users dev
$sql = "CREATE TABLE users_dev (
    id mediumint(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) default NULL,
    email VARCHAR(50) default NULL,
    password char(60) DEFAULT NULL,
    mobile varchar(255) DEFAULT NULL,
    name varchar(30) DEFAULT NULL,
    surname varchar(30) DEFAULT NULL,
    job_title varchar(255) DEFAULT NULL,
    bio text,
    UNIQUE KEY idx_email (email),
    UNIQUE KEY idx_mobile (mobile)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
    
    include_once('con.php');

    if (mysqli_query($con, $sql)) {
      echo "Table Users created successfully";
    } else {
      echo "Error creating table: " . mysqli_error($con);
    }
?>