<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="<?php echo $str_language; ?>" xml:lang="<?php echo $str_language; ?>">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Update User</title>
<link rel="stylesheet" href="/stylesheets/theme-style.css">
</head>
    <body>
        <div class="main back">
            <h1>Welcome to Scout</h1>
            <div class="overlay_nav users update">
            <?php 
                $id = $_GET['id'];
                $get_email = $_GET['email'];
            ?>
            <div class="user_update_form">
                <p>Updating details for user: <?php echo $get_email;?> </p>
                <form class="form" action="" method="post">
                    <div class="input_wrapper third">
                        <label>Username</label>
                        <input name="usern" type="text"/>
                    </div>
                    <div class="input_wrapper third">
                        <label>Mobile</label>
                        <input name="mobile" type="text"/>
                    </div>
                    <div class="input_wrapper third">
                        <label>Name</label>
                        <input name="name" type="text"/>
                    </div>
                    <div class="input_wrapper half">
                        <label>Surname</label>
                        <input name="surname" type="text"/>
                    </div>
                    <div class="input_wrapper half">
                        <label>Job Title</label>
                        <input name="job_title" type="text"/>
                    </div>
                    <label>Bio</label>
                    <textarea class="in" name="bio" type="text"></textarea>
                    <input class="sub" type="submit" value="Update" name="submit"/>
                </form>
            </div>
            </div>
        </div>
    </body>

<?php
    $usern = $_POST['usern'];
    $mobile = $_POST['mobile'];
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $jobTitle = $_POST['job_title'];
    $bio = $_POST['bio'];
    if(isset($_POST['submit'])){
        include_once('con.php');
        $sql = "UPDATE users_dev SET username='$usern', mobile='$mobile', name='$name', surname='$surname', job_title='$jobTitle', bio='$bio' WHERE id='$id'";
            if (mysqli_query($con, $sql)) {
                ?>
                <script>
                    alert("Record updated!")
                    window.location.href = "/users.php";
                </script>
                <?php
            } else {
                echo "Error: ".$sql."<br>".mysqli_error($con);
            }
            mysqli_close($con);
    }
?>
</html>