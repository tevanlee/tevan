<?php
    include_once('con.php');
    $sql = "SELECT * FROM users_dev";
    $result = mysqli_query($con, $sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="<?php echo $str_language; ?>" xml:lang="<?php echo $str_language; ?>">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>User List</title>
<link rel="stylesheet" href="/stylesheets/theme-style.css">
</head>
    <body>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <h2>Add new user:</h2>
                <p>Field with a (*) are required.</p>
                <form class="addUserForm" action="" method="post">
                    <div class="input_wrapper">
                        <label>Name (*)</label>
                        <input name="name" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Surname (*)</label>
                        <input name="surname" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Email (*)</label>
                        <input name="email" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Mobile (*)</label>
                        <input name="mobile" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Job Title (*)</label>
                        <input name="job_title" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Username (*)</label>
                        <input name="usern" type="text"/>
                    </div>
                    <div class="input_wrapper">
                        <label>Password (*)</label>
                        <input name="pass" type="password"/>
                    </div>
                    <div class="input_wrapper textarea">
                        <label>Bio</label>
                        <textarea class="in" name="bio" type="text"></textarea>
                    </div>
                    <input class="sub" type="submit" value="Add" name="submit"/>
                </form>
            </div>
        </div>
        <div class="main back">
            <h1>List of users</h1>
            <div class="overlay_nav users">
                <a class="update adduser" id="myBtn">Add User</a>
                <table>
                    <thead>
                        <td>Full Name</td>
                        <td>Email</td>
                        <td>Mobile</td>
                        <td>Options</td>
                    </thead>
                    <?php
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {?>
                                <tr>
                                    <td><?php echo $row['name']." ".$row['surname'];?></td>
                                    <td><?php echo $row['email'];?></td>
                                    <td><?php echo $row['mobile'];?></td>
                                    <td><a href="/update-user.php?id=<?php echo $row['id'];?>&email=<?php echo $row['email'];?>" class="update">Update</a></td>
                                </tr>
                            <?php
                            }
                        } 
                        // else{
                        //     echo "0 results";
                        // }
                        
                        $name = $_POST['name'];
                        $surn = $_POST['surname'];
                        $em = $_POST['email'];
                        $mob = $_POST['mobile'];
                        $job = $_POST['job_title'];
                        $un = $_POST['usern'];
                        $pass = $_POST['pass'];
                        $bio = $_POST['bio'];
                        if(isset($_POST['submit'])){
                            include_once('con.php');
                            $sql = "INSERT INTO users_dev VALUES (null, '$un', '$em', '$pass', '$mob', '$name', '$surn', '$job', '$bio')";
                            
                            //Check if email exists
                            $sql_find_email = "SELECT * FROM users_dev where email='$em'";
                            $count_email = mysqli_query($con, $sql_find_email);

                            //Check if username exists
                            $sql_find_un = "SELECT * FROM users_dev where username='$un'";
                            $count_un = mysqli_query($con, $sql_find_un);

                            //Check if passowrd exists
                            $sql_find_pass = "SELECT * FROM users_dev where password='$pass'";
                            $count_pass = mysqli_query($con, $sql_find_pass);

                            //Check if mobile exists
                            $sql_find_mob = "SELECT * FROM users_dev where mobile='$mob'";
                            $count_mob = mysqli_query($con, $sql_find_mob);

                            if(mysqli_num_rows($count_email) > 0){
                                ?>
                                <script>
                                    alert("Email already exists!")
                                    window.location.href = "/users.php";
                                </script>
                                <?php
                            }
                            else if(mysqli_num_rows($count_un) > 0){
                                ?>
                                <script>
                                    alert("Unsername already exists!")
                                    window.location.href = "/users.php";
                                </script>
                                <?php
                            }
                            else if(mysqli_num_rows($count_pass) > 0){
                                ?>
                                <script>
                                    alert("Password already exists!")
                                    window.location.href = "/users.php";
                                </script>
                                <?php
                            }
                            else if(mysqli_num_rows($count_mob) > 0){
                                ?>
                                <script>
                                    alert("Mobile already exists!")
                                    window.location.href = "/users.php";
                                </script>
                                <?php
                            }
                            else if($name == null || $surn == null || $em == null || $mob == null || $job == null || $un == null || $pass == null){
                                ?>
                                <script>
                                    alert("One or more required fields are blank")
                                    window.location.href = "/users.php";
                                </script>
                                <?php
                            }
                            else if (mysqli_query($con, $sql)) {
                                    
                                    ?>
                                    <script>
                                        alert("new record added!")
                                        window.location.href = "/users.php";
                                    </script>
                                    <?php
                                } else {
                                    echo "Error: ".$sql."<br>".mysqli_error($con);
                                }
                                mysqli_close($con);
                        }

                    ?>
                    
                </table>
            </div>
        </div>
    </body>
    <script src="/js/custom.js"></script>
</html>