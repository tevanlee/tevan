**Setup Instructions**

## Host
- Set up a hosting evironment with the ability to connect to a DB.

---

## Connect to DB and create table
- Configure the con.php file to include your DB details and test to connect, once connected run the create-table.php file to create the table required.

---

## Run test/host locally
- Navigate to the host that's been set up locally with name of your preference and proceed as you wish.

---

## If you'd like to make an CSS changes run these SASS Commands below to compile your CSS updates
- in terminal CD into stylesheets
- sass --watch sass/theme-style.scss:theme-style.css
---

**Enjoy thanks**