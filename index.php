<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="<?php echo $str_language; ?>" xml:lang="<?php echo $str_language; ?>">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Homepage</title>
<link rel="stylesheet" href="/stylesheets/theme-style.css">
</head>
    <body>
        <div class="main back">
            <h1>Welcome to Scout</h1>
            <div class="overlay_nav">
                <div class="column one">
                    <a href="/users.php">
                        <div class="image">
                            <img src="/images/usersIcon.png"/>
                            View Users
                        </div>
                    </a>
                </div>
                <div class="column two">
                    <a href="#/">
                        <div class="image">
                            <img src="/images/adduserIcon.png"/>
                            Add User
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>